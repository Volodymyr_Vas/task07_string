package com.vov4ik.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringUtils {
    private List<Object> parametersList = new ArrayList<>();
    public void addParameters(Object ... objects) {
        Collections.addAll(parametersList, objects);
    }

    public String concatObjects() {
        StringBuilder stringBuilder = new StringBuilder();
        if (parametersList != null) {
            parametersList.forEach(s -> stringBuilder.append(s).append(", "));
            return stringBuilder.toString();
        } else {
            return "You didn't set any parameters";
        }
    }
}
