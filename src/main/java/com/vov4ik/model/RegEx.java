package com.vov4ik.model;

import com.vov4ik.view.logger.MyLogger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
    private MyLogger logger = new MyLogger();

    public void checkSentence(String sentence) {
        Pattern sentencePattern = Pattern.compile("^[A-Z].*\\.$");
        Matcher sentenceMatcher = sentencePattern.matcher(sentence);
        if (sentenceMatcher.matches()) {
            logger.printInfo("This sentence is well written");
        } else {
            logger.printInfo("This sentence should be written better");
        }
    }

    public void splitString(String sentence) {
        String[] newSentences = sentence.split("the|you");
        for (String newSentence : newSentences) {
            logger.printInfo(newSentence);
        }
    }

    public void replaceVowels(String sentence) {
        logger.printInfo(sentence.replaceAll("[aoiue]", "_"));
    }
}
