package com.vov4ik.controller;

import com.vov4ik.model.RegEx;
import com.vov4ik.model.StringUtils;

public class MyController {
    public String useStringUtils(Object...objects){
        StringUtils stringUtils = new StringUtils();
        stringUtils.addParameters(objects);
        return stringUtils.concatObjects();
    }

    public void checkRegEx(String sentence) {
        RegEx regEx = new RegEx();
        regEx.checkSentence(sentence);
        regEx.splitString(sentence);
        regEx.replaceVowels(sentence);
    }
}
