package com.vov4ik.view;

import com.vov4ik.controller.MyController;
import com.vov4ik.view.logger.MyLogger;

import java.util.*;

public class MyView {
    private MyLogger logger = new MyLogger();
    private MyController controller = new MyController();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu () {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void setMethodsMenu(){
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showStringUtilsResult);
        methodsMenu.put("2", this::makeEnglishInternationalization);
        methodsMenu.put("3", this::makeUkrainianInternationalization);
        methodsMenu.put("4", this::makeGermanInternationalization);
        methodsMenu.put("5", this::showRegExWork);
    }

    public MyView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        setMethodsMenu();
    }

    private void showStringUtilsResult() {
        logger.printInfo(controller.useStringUtils(5, 12.5, "test1", "test2", new Object()));
    }

    private void makeEnglishInternationalization() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    private void makeUkrainianInternationalization() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    private void makeGermanInternationalization() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    public void showMenu() {
        String input;
        do {
            logger.printInfo("Menu:\n");
            for (String key : menu.keySet()) {
                logger.printInfo(menu.get(key));
            }
            logger.printInfo("Please, select one of points");
            input = scanner.nextLine().toUpperCase();
            if (input.equals("Q")) {
                break;
            }else try {
                methodsMenu.get(input).print();
            } catch (Exception e) {
                logger.printWarning("Your input was incorrect. \nPlease, try again");
            }
        } while (!input.equals("Q"));
    }

    private void showRegExWork(){
        controller.checkRegEx("This is the message you want to test.");
    }
}
